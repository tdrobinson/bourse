#!/usr/bin/env python3

from flask import Flask, json, request
import redis
import ystockquote as ysq

import datetime
import urllib
import os

app = Flask(__name__, static_folder='static', static_url_path='')
redis_store = redis.StrictRedis(host='localhost', port=6379, db=0)
if 'REDISCLOUD_URL' in os.environ:
    url = urllib.parse.urlparse(os.environ.get('REDISCLOUD_URL'))
    redis_store = redis.StrictRedis(host=url.hostname, port=url.port, password=url.password)

DATE_FORMAT = '%Y-%m-%d'
TTL_SECONDS = 86400
LAST_SAMPLE_SUFFIX = ':latest'
DATE_KEY = 'Date'


@app.route('/stocks/<stock>', methods=['GET'])
def get_stock(stock):
    try:
        _update_cache(stock)
        return json.jsonify(stock=stock, prices=_get_historical_prices(stock, 30))
    except urllib.error.HTTPError:
        return json.jsonify(error='stock {0} does not exist'.format(stock)), 404


def _update_cache(stock):
    up_to_date, from_date, to_date = _cache_up_to_date(stock)

    if not up_to_date:
        try:
            prices = _request_historical_prices(stock, from_date, to_date)

            for price in prices:
                redis_store.lpush(stock, json.dumps(price))
            redis_store.expire(stock, TTL_SECONDS)

            redis_store.set(_last_sample_key(stock), to_date)
            redis_store.expire(_last_sample_key(stock), TTL_SECONDS)
        except urllib.error.HTTPError:
            print('Unable to retrieve stock prices for', stock, 'from', from_date, 'to', to_date)


def _cache_up_to_date(stock):
    latest = redis_store.get(_last_sample_key(stock))

    now = datetime.date.today()
    before = now - datetime.timedelta(weeks=5)

    from_date = latest.decode('UTF-8') if latest else before.strftime(DATE_FORMAT)
    to_date = now.strftime(DATE_FORMAT)

    return from_date == to_date, from_date, to_date


def _request_historical_prices(stock, from_date, to_date):
    prices = ysq.get_historical_prices(stock, from_date, to_date)

    for date in prices:
        prices[date][DATE_KEY] = date

    return sorted(prices.values(), key=lambda price: price[DATE_KEY])


def _get_historical_prices(stock, samples):
    return [json.loads(s) for s in redis_store.lrange(stock, 0, samples)]


def _last_sample_key(stock):
    return stock + LAST_SAMPLE_SUFFIX


@app.route('/stocks/dash', methods=['POST'])
def get_dashboard():
    user_settings = request.get_json()

    if 'saved' in user_settings:
        for saved in user_settings['saved']:
            _update_cache(saved)

    if 'recent' in user_settings:
        for recent in user_settings['recent']:
            _update_cache(recent)
    
    return json.jsonify(saved=[{'stock': s, 'prices': _get_historical_prices(s, 30)}
                               for s in user_settings.get('saved') or []],

                        recent=[{'stock': s, 'prices': _get_historical_prices(s, 30)}
                                for s in user_settings.get('recent') or []])


@app.route('/', methods=['GET'])
def get_index():
    return app.send_static_file('index.html')

if __name__ == '__main__':
    app.run(port=5050, debug=True)
