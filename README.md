#Bourse

A simple stock signals dashboard.

[Demo](http://bourse-demo.herokuapp.com)

##Prerequisites
* Python 3
* Redis

##Install
Clone the repository, create and activate the virtualenv:

    git clone https://tdrobinson@bitbucket.org/tdrobinson/bourse.git
    cd bourse
    virtualenv3 .    # create virtualenv here - the command on your distro may differ
    . bin/activate

Install the required python libraries in the virtualenv:
 
    bin/pip install -r requirements.txt

Ensure Redis is started

    sudo systemctl start redis   # again the command on your distro may differ

Start bourse

    bin/python bourse.py
