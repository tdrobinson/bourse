var Http = (function() {

    var http = {};

    http.get = function(url, success, failure, error) {
        var request = new XMLHttpRequest();
        request.open('GET', url, true);

        request.onload = function() {
            var data = JSON.parse(request.responseText);

            if (request.status >= 200 && request.status < 400) {
                success(data);
            } else {
                failure(data);
            }
        };

        request.onerror = error;

        request.send();
    };

    http.post = function(url, data, success, failure, error) {
        var request = new XMLHttpRequest();
        request.open('POST', url, true);
        request.setRequestHeader('Content-Type', 'application/json');

        request.onload = function() {
            var data = JSON.parse(request.responseText);

            if (request.status >= 200 && request.status < 400) {
                success(data);
            } else {
                failure(data);
            }
        };

        request.onerror = error;

        request.send(JSON.stringify(data));
    };

    return http;

})();
