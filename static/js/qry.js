var Q = (function() {

    var q = {};
    
    q.addClass = function(id, className) {
        var el = document.getElementById(id);

        if (el.classList) {
            el.classList.add(className);
        } else {
            el.className += ' ' + className;
        }
    };

    q.removeClass = function(id, className) {
        var el = document.getElementById(id);

        if (el.classList) {
          el.classList.remove(className);
        } else {
          el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        }
    };

    return q;

})();
