
(function() {

    var bourse = new Ractive({
        el: 'bourse-app',
        template: '#bourse-main',
        data: {
            line: function(toPlot, xScale, yScale) {
                var min = Math.min.apply(null, toPlot);
                var max = Math.max.apply(null, toPlot);

                var results = toPlot.map(function(price, i) {
                    return xScale(i, 0, toPlot.length) + ',' + yScale(price, min * 0.99, max * 1.01);
                });

                return results.join(' ');
            },
            closes: function(prices) {
                return prices.map(function(price, i) {
                    return parseFloat(price['Close']);
                });
            }
        },
        components: {
            'stock-chart': Ractive.extend({
                template: '#stock-chart-widget',
                onrender: function() {
                    var widget = this;

                    var resize = function() {
                        var chart = document.getElementById('detail-chart');
                        if (chart) {
                            var width = chart.clientWidth;

                            if (width > 0) {
                                widget.set({
                                    'width': width,
                                    'height': width / 2
                                });
                            }
                        }
                    };

                    var linearScale = function (range) {
                      return function (value, d0, d1) {
                        return range[0] + ( ( value - d0 ) * ( range[1] - range[0] ) / ( d1 - d0 ) );
                      };
                    };

                    this.observe({
                        'width': function(width) {
                            this.set('xScale', linearScale([0, width]));
                        },
                        'height': function(height) {
                            this.set('yScale', linearScale([0, height]));
                        }
                    });

                    window.addEventListener('resize', resize);
                },
                data: {
                    width: 0,
                    height: 0
                }
            }),
            'stock-lists': Ractive.extend({
                template: '#stock-lists-widget',
                onrender: function() {
                    var saved = JSON.parse(localStorage.getItem('bourse.saved')) || ['ATM.NZ', 'DIL.NZ', 'TTK.NZ' ];
                    var recent = JSON.parse(localStorage.getItem('bourse.recent')) || [ 'GOOGL', 'MSFT' ];
                    var settings = {'saved': saved, 'recent': recent};

                    var success = function(data) {
                        Q.addClass('loader', 'hidden');
                        Q.removeClass('content', 'hidden');
                        Q.removeClass('search', 'hidden');

                        bourse.set('saved', data.saved);
                        bourse.set('recent', data.recent);
                        window.dispatchEvent(new Event('resize'));
                    };

                    var failure = function(data) {
                        Q.addClass('loader', 'hidden');
                    };

                    Http.post('/stocks/dash', settings, success, failure);
                }
            }),
            'stock-list': Ractive.extend({
                template: '#stock-list-widget',
                onrender: function () {
                    var widget = this;

                    var resize = function() {
                        resizeCharts(widget.get('name').toLowerCase());
                    };

                    var resizeCharts = function(chart) {
                        var width = document.getElementById(chart + '-chart-column').clientWidth;
                        var height = document.getElementById(chart + '-chart-column').clientHeight;

                        if (width > 0 && height > 0) {
                            widget.set({
                                'width': width - 10,
                                'height': height
                            });
                        }
                    };

                    var linearScale = function (range) {
                      return function (value, d0, d1) {
                        return range[0] + ( ( value - d0 ) * ( range[1] - range[0] ) / ( d1 - d0 ) );
                      };
                    };

                    this.observe({
                        'width': function(width) {
                            this.set('xScale', linearScale([0, width]));
                        },
                        'height': function(height) {
                            this.set('yScale', linearScale([0, height]));
                        }
                    });

                    this.on({
                        'selected': function(event, i) {
                            var needsResizing = !bourse.get('selected');
                            bourse.set('selected', widget.get('stocks')[i]);

                            if (needsResizing) {
                                window.dispatchEvent(new Event('resize'));
                            }
                        }
                    });

                    window.addEventListener('resize', resize);
                },
                save: function(stock, pos) {
                    bourse.get('saved').push(stock);
                },
                remove: function(stock, pos) {
                    bourse.get('saved').splice(pos, 1);
                },
                data: {
                    width: 0,
                    height: 0,
                    last_close: function(prices) {
                        return prices[prices.length - 1]['Close'];
                    }
                }
            })
        }
    });

    var saveUserSettings = function(newSettings, oldSettings, key) {
        if (newSettings) {
            var newStocks = newSettings.map(function(s) {
                return s['stock'];
            });
            localStorage.setItem('bourse.' + key, JSON.stringify(newStocks));
        }
    };

    bourse.observe({
        'saved': saveUserSettings,
        'recent': saveUserSettings
    });

    var toggleSearch = function(searching) {
        var search = document.getElementById('search');
        if (searching) {
            search.setAttribute('disabled', 'disabled');
        } else {
            search.removeAttribute('disabled');
        }
    };

    var updateRecent = function(data) {
        if (data.prices.length > 0) {
            var length = bourse.get('recent').unshift(data);
            if (length > 5)
            {
                bourse.get('recent').splice(5, length - 5);
            }
        }
        toggleSearch(false);
    };

    bourse.on('search', function(event) {
        toggleSearch(true);
        Http.get('/stocks/' + bourse.get('search'), updateRecent);
        event.original.preventDefault();
    });

})();